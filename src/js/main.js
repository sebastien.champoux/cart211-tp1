import MenuToggle from "./src/Components/MenuToggle";
import PostsList from "./src/Components/PostsList";
import OpeningAnimation from "./src/OpeningAnimation";
import ToggleMenuCmd from "./src/Commands/ToggleMenuCmd";
import Menu from "./src/Components/Menu";
import Aside from "./src/Components/Aside";
import CloseAsideDecorator from "./src/Commands/CloseAsideDecorator";
import StartVideoCmd from "./src/Commands/StartVideoCmd";
import ScrollToTop from "./src/Components/ScrollToTop";
import ScrollToAnchor from "./src/Commands/ScrollToAnchor";

document.addEventListener('DOMContentLoaded', e => {
	const postsList = new PostsList(document.querySelector('.js-posts-list'));
	const aside = new Aside(document.querySelector('.js-aside'));
	const menu = new Menu(document.querySelector('.js-menu'));
	const menuToggle = new MenuToggle(document.querySelector('.js-menu-toggle'));
	const scrollToTop = new ScrollToTop(document.querySelector('.js-scroll-to-top'));
	
	// Setup everything
	menuToggle.setClickCommand(new ToggleMenuCmd(menuToggle, aside, menu));
	aside.setupInitialState();
	menu.setupMenu(postsList);
	menu.getMenuLinks().forEach(link => {
		var closeAsideDecorator = new CloseAsideDecorator(link.clickCmd, menu, aside, menuToggle);
		link.clickCmd = closeAsideDecorator;
	});
	scrollToTop.setClickCmd(new ScrollToAnchor(0, 1));

	const openingAnimation = new OpeningAnimation();
	openingAnimation.animateInPage();

	// Video player
	let startVideoCmd = new StartVideoCmd(aside, menuToggle, 83);
	document.addEventListener('keydown', e => startVideoCmd.execute(e), false);
});