import Post from './Components/Post';

/**
 * This doesn't adhere very well to DRY principles
 * but JavaScript isn't evaluated in this project
 */
export default class OpeningAnimation {
    constructor() {
        this.body = document.querySelector('body');

        this.menuToggle = document.querySelector('.js-menu-toggle');
        this.menuToggleBG = this.menuToggle.querySelector('.js-menu-toggle__bg');
        this.burger = this.menuToggle.querySelector('.js-hamburger');

        this.articlesList = document.querySelector('.js-posts-list');
        this.articlesListBG = this.articlesList.querySelector('.js-posts-list__bg');

        this.mainPageTitle = document.querySelector('.js-main__page-title');

        this.posts = Array.from(document.querySelectorAll('.js-post'));
        this.posts = this.posts.map(post => new Post(post));
    }

    animateInPage() {
        var tl = new TimelineMax({ delay: 0.4 });

        tl.addLabel('setup');
        tl.set(this.articlesList, { transformOrigin: '0 0' });
        tl.set(this.articlesListBG, { transformOrigin: '0 0' });
        tl.set(this.menuToggle, { transformOrigin: '0 0' });
        tl.set(this.menuToggleBG, { transformOrigin: '0 100%' });

        tl.addLabel('animate-in-line');
        tl.from(this.articlesList, 0.5, {
            scaleX: 0,
            ease: Power0.easeNone
        });
        tl.from(this.menuToggle, 0.3, { scaleX: 0 });

        tl.addLabel('animate-in-title');
        tl.from(this.mainPageTitle, 1.3, { y: 150, ease: Elastic.easeOut.config(1, 1) });

        tl.addLabel('animate-in-menu-toggle', 'animate-in-title+=0.4');
        tl.from(this.menuToggleBG, 0.6, { scaleY: 0 }, 'animate-in-menu-toggle');
        tl.from(this.burger, 0.3, { opacity: 0 }, 'animate-in-menu-toggle+=0.2');
        
        tl.addLabel('animate-in-posts-list', 'animate-in-menu-toggle+=0.1');
        tl.from(this.articlesListBG, 5, { scaleY: 0 }, 'animate-in-posts-list');
        tl.add(
            this.posts.map(p => p.animateIn()),
            'animate-in-posts-list+=0.3',
            'start',
            0.5
        );

        return tl;
    }
}