import { playlist } from '../Playlist';
import CloseVideoCmd from './CloseVideoCmd';

export default class StartVideoCmd {
	constructor(aside, menuToggle, keycodeTrigger = 83) {
		this.aside = aside;
		this.menuToggle = menuToggle;
		this.keycodeTrigger = keycodeTrigger;
	}

	execute(e) {
		if (e.keyCode === this.keycodeTrigger) {
			let tl = new TimelineMax();
			tl.addCallback(this.menuToggle.toggleBurger.bind(this.menuToggle));
			tl.add(this.aside.expandAside('100%'));
			tl.addCallback(this.changeMenuToggleCmd.bind(this));
			tl.addCallback(this.createVideo.bind(this));
			return tl;
		}
	}

	createVideo() {
		const videoIndex = Math.floor(Math.random() * playlist.length);
		this.aside.createVideo(playlist[videoIndex]);
	}

	changeMenuToggleCmd() {
		const oldMenuToggleCmd = this.menuToggle.clickCommand;
		this.menuToggle.setClickCommand(new CloseVideoCmd(this.aside, this.menuToggle, oldMenuToggleCmd));
	}
};
