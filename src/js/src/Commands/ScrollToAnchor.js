export default class ScrollToAnchor {
    constructor(anchorHref, animationSpeed) {
        this.anchorHref = anchorHref;
        this.animationSpeed = animationSpeed;
    }

    execute(e) {
        e.preventDefault();
        TweenMax.to(
            window,
            this.animationSpeed,
            { scrollTo: this.anchorHref }
        );
    }
};
