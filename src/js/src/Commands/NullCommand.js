export default class NullCommand {
    execute() {
        console.warn('Null command executed.  This is probably not the expected behavior.');
    }
};
