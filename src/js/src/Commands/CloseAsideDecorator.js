/**
 * Command decorator that closes the aside after executing the decorated's command
 */
export default class CloseAsideDecorator {
	/**
	 * @param {Command} decorated 
	 * @param {Menu} menu 
	 * @param {Aside} aside 
	 */
	constructor(decorated, menu, aside, menuToggle) {
		this.decorated = decorated;
		this.menu = menu;
		this.aside = aside;
		this.menuToggle = menuToggle;
	}

	execute(e) {
		this.decorated.execute(e);
		// Close burger
		if (this.menuToggle.isActive()) {
			this.menuToggle.toggleBurger();
		}
		// Close aside
		let tl = new TimelineMax();
		tl.addLabel('hide-stuff-in-aside')
		tl.add(this.menu.closeMenu(), 'hide-stuff-in-aside');
		tl.add(this.aside.hideSecretMessage(), 'hide-stuff-in-aside');
		tl.add(this.aside.closeAside());
		return tl;
	}
};
