export default class CloseVideoCmd {
	constructor(aside, menuToggle, oldMenuToggleCmd) {
		this.aside = aside;
		this.menuToggle = menuToggle;
		this.oldMenuToggleCmd = oldMenuToggleCmd;
	}

	execute(e) {
		let tl = new TimelineMax();
		tl.add(this.aside.deleteVideo());

		tl.addLabel('close-aside', '+=0.2');
		tl.addCallback(() => this.menuToggle.toggleBurger(), 'close-aside');
		tl.add(this.aside.closeAside(), 'close-aside');

		tl.addCallback(() => this.menuToggle.setClickCommand(this.oldMenuToggleCmd));
		return tl;
	}
};
