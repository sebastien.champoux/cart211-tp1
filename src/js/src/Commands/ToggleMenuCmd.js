export default class ToggleMenuCmd {
	/**
	 * @param {MenuToggle} menuToggle 
	 */
	constructor(menuToggle, aside, menu) {
		this.menuToggle = menuToggle;
		this.aside = aside;
		this.menu = menu;
	}

	execute(e) {
		if (this.menuIsOpen()) {
			this.closeMenu();
		} else {
			this.openMenu();
		}
		this.menuToggle.toggleBurger();
	}

	menuIsOpen() {
		return this.menuToggle.isActive();
	}

	openMenu() {
		let tl = new TimelineMax();
		tl.add(this.aside.expandAside());
		tl.addLabel('appear-stuff-in-aside', '-=0.4')
		tl.add(this.menu.openMenu(), 'appear-stuff-in-aside');
		tl.add(this.aside.displaySecretMessage(), 'appear-stuff-in-aside+=0.3');
		return tl;
	}
	
	closeMenu() {
		let tl = new TimelineMax();
		tl.addLabel('hide-content');
		tl.add(this.aside.hideSecretMessage(), 'hide-content');
		tl.add(this.menu.closeMenu(), 'hide-content');
		tl.add(this.aside.closeAside());
		return tl;
	}
};
