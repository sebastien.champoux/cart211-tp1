export default class Aside {
	constructor(rootElement) {
		this.root = rootElement;
		this.content = rootElement.querySelector('.js-aside__content');
		this.videoWrapper = rootElement.querySelector('.js-aside__video-container');
		this.videoPlaceholder = rootElement.querySelector('.js-aside__video-placeholder');
		this.secretMessage = rootElement.querySelector('.js-aside__message');
	}

	setupInitialState() {
		TweenMax.set(this.content, {
			scaleY: 0,
			transformOrigin: '0 0'
		});
		TweenMax.set(this.secretMessage, { display: 'none', opacity: 0 });
	}

	expandAside(expandedWidth = '25%') {
		this.initialWidth = this.root.getBoundingClientRect().width;

		let tl = new TimelineMax();
		tl.to(this.content, 1, { scaleY: 1 });
		tl.to(this.root, 0.5, { width: expandedWidth }, '-=0.7');
		return tl;
	}

	closeAside() {
		let tl = new TimelineMax();
		tl.to(this.root, 0.5, { width: this.initialWidth });
		tl.to(this.content, 0.5, { scaleY: 0 }, '-=0.3');
		return tl;
	}

	displaySecretMessage() {
		return TweenMax.to(this.secretMessage, 0.6, { display: 'block', opacity: 1 });
	}
	
	hideSecretMessage() {
		return TweenMax.to(this.secretMessage, 0.6, { display: 'none', opacity: 0 });
	}

	/** @param {Object} videoMeta metadata of the video, from Playlist.js */
	createVideo(videoMeta) {
		const playerDimensions = this.videoWrapper.getBoundingClientRect();
		const playerVars = {
			autoplay: 1,
		};
		if (videoMeta.start) {
			playerVars.start = videoMeta.start;
		}
		this.player = new YT.Player(this.videoPlaceholder, {
			width: playerDimensions.width,
			height: playerDimensions.height,
			videoId: videoMeta.id,
			playerVars: playerVars
		});
		console.log(this.player);
	}

	deleteVideo() {
		let tl = new TimelineMax();
		tl.to(this.player.a, 0.8, { opacity: 0, display: 'none' });
		tl.addCallback(() => {
			const videoParentNode = this.player.a.parentNode;
			videoParentNode.removeChild(this.player.a);
			this.videoPlaceholder = document.createElement('div');
			videoParentNode.appendChild(this.videoPlaceholder);
		});
		return tl;
	}
};
