import ScrollToAnchor from "../Commands/ScrollToAnchor";

export default class Menu {
    constructor(rootElement) {
		this.root = rootElement;
		this.menuItems = [];
    }

	/** @param {PostsList} postsList */
	setupMenu(postsList) {
		// Get and remove template
		const menuItemTemplate = this.getMenuItemTemplate();
		menuItemTemplate.parentNode.removeChild(menuItemTemplate);
		this.menuItemDisplay = getComputedStyle(menuItemTemplate).display;

		let cloneItem, postId, label;
		postsList.getPostsNodes().forEach(post => {
			postId = post.id;
			label = post.getAttribute('data-menu-label');

			cloneItem = this.makeMenuItem(menuItemTemplate, label, `#${postId}`);
			this.root.appendChild(cloneItem);
			this.menuItems.push(cloneItem);

			this.attachEventToItem(cloneItem);
		});

		// Hide the menu items by default
		TweenMax.set(this.menuItems, { opacity: 0, display: 'none' });
	}

	getMenuItemTemplate() {
		const menuItemTemplate = this.root.querySelector('.js-menu__item--template');
		menuItemTemplate.classList.remove('js-menu__item--template');
		return menuItemTemplate;
	}

	/**
	 * @param {HTMLElement} itemTemplate
	 * @param {String} label
	 * @param {String} href
	 */
	makeMenuItem(itemTemplate, label, href) {
		const clone = itemTemplate.cloneNode(true);
		const link = clone.querySelector('.js-menu__link');
		clone.linkNode = link;
		link.setAttribute('href', href);
		link.innerText = label;

		return clone;
	}

	/** @param {HTMLElement} menuItemNode */
	attachEventToItem(menuItemNode) {
		const link = menuItemNode.linkNode;
		const clickCmd = new ScrollToAnchor(link.getAttribute('href'), 1);
		link.clickCmd = clickCmd;
		link.addEventListener('click', e => {
			link.clickCmd.execute(e);
		});
	}

	openMenu() {
		let tl = new TimelineMax();
		tl.staggerFromTo(
			this.menuItems,
			0.5,
			{ opacity: 0, display: 'none' },
			{ opacity: 1, display: this.menuItemDisplay },
			0.1
		);
		return tl;
	}

	closeMenu() {
		let tl = new TimelineMax();
		tl.to(this.menuItems, 0.3, { opacity: 0, display: 'none' });
		return tl;
	}

	getMenuItems() {
		return this.menuItems;
	}

	getMenuLinks() {
		return this.menuItems.map(item => item.linkNode);
	}
};
