import NullCommand from "../Commands/NullCommand";

export default class ScrollToTop {
	constructor(rootElement) {
		this.root = rootElement;
		this.clickCmd = new NullCommand();
		this.apparitionPoint = 150;
		this.isVisible = window.scrollY >= this.apparitionPoint;
		this.originalDisplay = getComputedStyle(this.root).display;

		this.setInitialState();
		this.attachEvents();
	}

	setInitialState() {
		if (!this.isVisible) {
			TweenMax.set(this.root, { opacity: 0, display: 'none' });
		}
	}
	
	attachEvents() {
		this.root.addEventListener('click', e => this.clickCmd.execute(e));
		window.addEventListener('scroll', this.toggleVisibility.bind(this));
	}

	toggleVisibility() {
		if (!this.isVisible && window.scrollY >= this.apparitionPoint) {
			this.isVisible = true;
			TweenMax.to(this.root, 0.6, { opacity: 1, display: this.originalDisplay });
		} else if (this.isVisible && window.scrollY < this.apparitionPoint) {
			this.isVisible = false;
			TweenMax.to(this.root, 0.6, { opacity: 0, display: 'none' });
		}
	}

	setClickCmd(command) {
		this.clickCmd = command;
	}
};
