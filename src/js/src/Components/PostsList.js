export default class PostsList {
    constructor(rootElement) {
        this.root = rootElement;
        this.bg = rootElement.querySelector('.js-posts-list__bg');
        this.posts = rootElement.querySelectorAll('.js-post');
    }

    getPostsNodes() {
        return this.posts;
    }

    animateIn() {
        const bgHeight = this.bg.getBoundingClientRect().height;
        const bgTweenLength = bgHeight * 0.4 / 500;

        var tl = new TimelineMax();
        tl.set(this.root, { transformOrigin: '0 50%' });
        tl.set(this.bg, { transformOrigin: '0 0' });
        tl.from(this.root, 0.6, { scaleX: 0 });
        tl.from(this.bg, bgTweenLength, { scaleY: 0 });
        return tl;
    }
};
