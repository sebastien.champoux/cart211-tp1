export default class Post {
    constructor(rootElement) {
        this.root = rootElement;
        this.postHeader = rootElement.querySelector('.js-post__header');
    }

    animateIn() {
        var tl = new TimelineMax();
        tl.from(this.root, 1, { opacity: 0 });
        return tl;
    }
};
