import NullCommand from '../Commands/NullCommand';

export default class MenuToggle {
	/**
	 * @param {HTMLElement} rootElement 
	 */
	constructor(rootElement) {
		this.root = rootElement;
		this.clickCommand = new NullCommand();
		this.bg = rootElement.querySelector('.js-menu-toggle__bg');
		this.burger = rootElement.querySelector('.js-hamburger');

		this.attachEvents();
	}

	setClickCommand(command) {
		this.clickCommand = command;
	}

	attachEvents() {
		const cmd = this.clickCommand;
		this.root.addEventListener('click', e => this.clickCommand.execute(e));
	}

	animateIn() {
		const tl = new TimelineMax();
		tl.set(this.root, { transformOrigin: '0 0' });
		tl.set(this.bg, { transformOrigin: '50% 100%' });
		tl.from(this.root, 0.4, { scaleX: 0 });
		tl.from(this.bg, 0.4, { scaleY: 0 });
		tl.from(this.burger, 0.3, { opacity: 0 }, '-=0.3');
		return tl;
	}

	isActive() {
		return this.burger.classList.contains('is-active');
	}

	toggleBurger() {
		this.burger.classList.toggle('is-active');
	}
}